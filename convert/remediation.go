package convert

import (
	"encoding/base64"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func (r result) toRemediation() (*issue.Remediation, error) {
	if r.FixedBy == "" {
		return nil, nil
	}

	diff, err := r.remediator.Diff(r.NamespaceName, r.FeatureName, r.FixedBy)
	if err != nil {
		return nil, err
	}

	base64EncodedDiff := base64.StdEncoding.EncodeToString([]byte(diff))
	vuln := r.toIssue()

	return &issue.Remediation{
		Summary: r.solution(),
		Diff:    base64EncodedDiff,
		Fixes: []issue.Ref{
			issue.NewRef(*vuln),
		},
	}, nil
}
