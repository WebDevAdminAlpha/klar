# klar analyzer changelog

## v3.1.2

- Fix bug which prevented klar version from being reported in the log output (!82)

## v3.1.1

- Fix bug which prevented `--version` from working (!81)

## v3.1.0

- Update common to v2.22.0 (!80)
- Update urfave/cli to v2.3.0 (!80)

## v3.0.1

- Fix default Docker image CMD to run the analyzer. (!77)

## v3.0.0

- Change Docker base image from `golang:$GO_VERSION-alpine` to `centos:centos8` (!68)

## v2.6.0

- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!60)

## v2.5.2

- Use scanner instead of analyzer in `scan.scanner` object (!56)

## v2.5.1

- Update Clair version (!55)

## v2.5.0

- Add scan object to report (!54)

## v2.4.9

- Improve error message when using a registry with a bad (or self-signed) SSL certificate (!52)

## v2.4.8

- Change Whitelist terms to Allowlist (!49)

## v2.4.7

- Add support for SECURE_LOG_LEVEL env var (!47)
- Change the date format for logs to use RFC3339 (!47)

## v2.4.6

- Replace local logging package with logutil package from GitLab common project (!46)

## v2.4.5

- Update to clair server v2.1.3

## v2.4.4

- remove `sortRemediations()` and use the updated common/issue lib that contains this sort (!42)

## v2.4.3

- Update pq library to 1.4.0 (!41)
- Update github.com/pkg/errors library to 0.9.1 (!41)
- Update sirupsen/logrus library to 1.5.0 (!41)
- Update olekukonko/tablewriter library to 0.0.4 (!41)
- Update urfave/cli library to 1.22.4 (!41)
- Update security-products/analyzers/common library to 2.9.1 (!41)
- Update go-yaml/yaml library to 2.2.8 (!41)

## v2.4.2

- Update go version to 1.14 (!40)
- Update alpine base image to 3.11.3 (!40)

## v2.4.1

- Ensure hostnames are first resolved using entries in `/etc/hosts` before executing a DNS lookup using an external nameserver

## v2.4.0

- Add `id` JSON field to vulnerabilities (!31)

## v2.3.2

- Update pq library to 1.3.0 (!36)

## v2.3.1

- Fix bug causing zero vulnerabilities on Red Hat based images (!34)
- Add `CLAIR_TRACE` environment variable for enabling full output from `clair` server process during scan (!34)

## v2.3.0

- Add support for custom CA certs (!30)

## v2.2.1

- Improve error message when the Docker container image cannot be found

## v2.2.0

- Expose `DOCKER_INSECURE` klar environment variable

## v2.1.0

- Add `CLAIR_DB_CONNECTION_STRING` variable, to set the connection string for the vulnerabilities database
- Deprecate `CLAIR_VULNERABILITIES_DB_URL` variable

## v2.0.3

- Output remediation fields

## v2.0.2

- Update to common library v2.5.4

## v2.0.1

- Update to clair server v2.1.2
- Execute clair server as a subprocess within the analyzer

## v2.0.0

- Rewrite converter code from `Node.js` to `Go`
- Output container scanning report using the [Security Products Common Format](https://gitlab.com/gitlab-org/security-products/analyzers/common)

## v1.0.3

- Add default values for DOCKER_USER/DOCKER_PASSWORD to allow using private GitLab Container Registry without additional configuration

## v1.0.2

- Fix whitelist handling by using the `clair-whitelist.yml` file instead of `clair-whitelist.yaml`
- Don't use the image version when matching against image names in the whitelist file

## v1.0.1

- Fix Kubernetes support by setting Clair DB URL to `127.0.01` when running in the context of a kubernetes executor (!6)

## v1.0.0

- Initial release
